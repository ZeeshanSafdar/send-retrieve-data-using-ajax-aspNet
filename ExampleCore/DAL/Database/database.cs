﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ExampleCore.Models;
using Microsoft.EntityFrameworkCore;

namespace ExampleCore.DAL.Database
{
    public class database : DbContext
    {
        public database(DbContextOptions<database> options) : base(options)
        {

        }

            
        public DbSet<User> Users { get; set; }
    }
}
