﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ExampleCore.DAL.Database;
using ExampleCore.Models;
using Microsoft.AspNetCore.Mvc;

namespace ExampleCore.Controllers
{
    public class UserController : Controller
    {
        private readonly database _database;
        // dependency injection
        public UserController(database db)
        {
            _database = db;
        }
        public IActionResult Index()
        {
            return View(_database.Users.ToList());
        }

        public ActionResult AddUser()
        {

            return View();
        }

        [HttpPost]
        public bool AddUser(User user)
        {
            if (!_database.Users.Any(u => u.Email == user.Email))
            {
                _database.Users.Add(user);
                _database.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }

        }

        public JsonResult GetUserData()
        {
            var user = _database.Users.Select(x => new
            {
                id = x.Id,
                name = x.Name,
                address = x.Address,
                email = x.Email
            }).ToList();

            //var data = _database.Users.ToList();
            return Json(user);
        }
    }
}